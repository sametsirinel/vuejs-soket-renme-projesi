var app = require("express")();
var http = require("http").createServer(app);
var io = require("socket.io")(http);

var USERS = {};
var MESSAGES = [];

app.get("/", (req, res) => {
  res.send("<h1>Hello world</h1>");
});

io.on("connection", (socket) => {
  io.emit("onlineChanged", USERS);
  io.emit("messageChanged", MESSAGES);
  socket.on("disconnect", () => {
    USERS[socket.id] = undefined;
    io.emit("onlineChanged", JSON.parse(JSON.stringify(USERS)));
  });

  socket.on("message", (msg) => {
    console.log("message: " + msg);
  });

  socket.on("joinWithUsername", (msg) => {
    USERS[socket.id] = msg;
    io.emit("onlineChanged", USERS);
  });

  socket.on("sendMessage", (msg) => {
    MESSAGES.push({
      name: USERS[socket.id],
      message: msg,
      time: new Date(),
    });
    io.emit("messageChanged", MESSAGES);
  });
});

http.listen(3000, () => {
  console.log("listening on *:3000");
});
