import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    login: false,
    username: "",
    socket: null,
  },
  mutations: {
    login(state, data) {
      state.login = data.login;
      state.username = data.username;
      state.socket = data.socket;
    },
  },
});

export default store;
